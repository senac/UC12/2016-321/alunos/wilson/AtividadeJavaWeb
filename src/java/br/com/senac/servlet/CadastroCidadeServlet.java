/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.servlet;

import br.com.senac.banco.CidadeDAO;
import br.com.senac.modelo.Cidade;
import br.com.senac.modelo.Pais;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrador
 */
public class CadastroCidadeServlet extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        int codigo = Integer.parseInt(request.getParameter("codigo"));
        
        CidadeDAO dao = new CidadeDAO();
        
        Cidade cidade = dao.buscarPorId(codigo);
        
        PrintWriter printWriter = response.getWriter();
        
        printWriter.print(cidade.getPais().getNome());
        
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        int codigo = Integer.parseInt(request.getParameter("codigo"));
        String nomeCidade = request.getParameter("cidade");
        String nomePais = request.getParameter("pais");
        
        Pais pais = new Pais();
        pais.setNome(nomePais);
        
        Cidade cidade = new Cidade();
        cidade.setCodigo(codigo);
        cidade.setNome(nomeCidade);
        cidade.setPais(pais);
        
        CidadeDAO dao = new CidadeDAO();
        
        dao.salvar(cidade);
        
        request.setAttribute("cidade", cidade);
        
        RequestDispatcher dispatcher = request.getRequestDispatcher("");
        
        dispatcher.forward(request, response);
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
