/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.servlet;

import br.com.senac.banco.CidadeDAO;
import br.com.senac.banco.ClienteDAO;
import br.com.senac.modelo.Cidade;
import br.com.senac.modelo.Cliente;
import br.com.senac.modelo.Endereco;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author wilso
 */
public class CadastroClienteServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CadastroClienteServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CadastroClienteServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      
     int codigo = Integer.parseInt(request.getParameter("codigo"));
     String primeiroNome = request.getParameter("promeiroNome");
     String ultimoNome = request.getParameter("ultimoNome");
     String email = request.getParameter("email");
     String telefone = request.getParameter("telefone");
     String cep = request.getParameter("cep");
     String logradouro = request.getParameter("endereco");
     String complemento = request.getParameter("complemento");
     String distrito = request.getParameter("distrito");
     int codigocidade = Integer.parseInt(request.getParameter("cidade"));
     
        Cliente cliente = new Cliente();
        cliente.setCodigo(codigo);
        cliente.setPrimeiroNome(primeiroNome);
        cliente.setUltimoNome(ultimoNome);
        cliente.setEmail(email);
        
        Endereco endereco = new Endereco();
        endereco.setLogradouro(logradouro);
        endereco.setComplemento(complemento);
        endereco.setDistrito(distrito);
        endereco.setTelefone(telefone);
        endereco.setCep(cep);
        
        CidadeDAO dao = new CidadeDAO();
        
        Cidade ci = dao.buscarPorId(codigocidade);
        
        endereco.setCidade(ci);
        
        cliente.setEndereco(endereco);
        
        ClienteDAO clienteDAO = new ClienteDAO();
        
        clienteDAO.salvar(cliente);
        
        RequestDispatcher dispatcher = request.getRequestDispatcher("/cliente.do");
         
        request.setAttribute("cliente", cliente);
        
        dispatcher.forward(request, response);
     
     
     
     
     
     
        
        
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
