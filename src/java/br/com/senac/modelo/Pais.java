/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.modelo;

import java.io.Serializable;

/**
 *
 * @author wilso
 */
public class Pais implements Serializable{
    
    private int codigo;
    private String Nome;

    public Pais() {
    }

    public Pais(int codigo, String Nome) {
        this.codigo = codigo;
        this.Nome = Nome;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String Nome) {
        this.Nome = Nome;
    }
    
    


}
