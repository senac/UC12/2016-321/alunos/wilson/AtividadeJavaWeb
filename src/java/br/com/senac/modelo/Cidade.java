/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.modelo;

import java.io.Serializable;

/**
 *
 * @author wilso
 */
public class Cidade implements Serializable{
    
    private int codigo;
    private String Nome;
    private Pais pais;
    public String getCodigo;

    public Cidade() {
    }

    public Cidade(int codigo, String Nome, Pais pais) {
        this.codigo = codigo;
        this.Nome = Nome;
        this.pais = pais;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }
    
    
    
}
