/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.banco;

import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author wilso
 */
public class Conexao {

    private static final String url = "jdbc:mysql://localhost/sakila";
    private static final String user = "root";
    private static final String password = "123456";
    private static final String DRIVER = "com.mysql.jdbc.Driver";

    public static Connection getConnection() {

        Connection connection = null;
        try {

            Class.forName(DRIVER);

            connection = (Connection) DriverManager.getConnection(url, user, password);

            System.out.println("Connectado com sucesso!!!");

        } catch (ClassNotFoundException ex) {
            System.out.println("Falha ao carregar o Driver do banco....");
        } catch (SQLException ex) {

        }

        return connection;
    }
    
    public static void main(String... a ){
        Conexao.getConnection();
    }

}
