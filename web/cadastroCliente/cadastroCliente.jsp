<%@page import="br.com.senac.modelo.Cidade"%>
<%@page import="java.util.List"%>
<%@page import="br.com.senac.banco.CidadeDAO"%>
<jsp:include page="../header.jsp"/>






<div class="container">
    <fieldset>
        <legend>Cadastro de Clientes</legend>

        <form class="form-horizontal" action="./cliente.do" method="post">

            <input type="hidden" name="codigo" value="0" />

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Dados Pessoais</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="codigo">C�digo:</label>
                        <div class="col-sm-2">
                            <input readonly="true"   type="text" class="form-control" id="codigo">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="primeiroNome">Primeiro nome:</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="primeiroNome" placeholder="Preencha campo primeiro nome" name="primeiroNome">
                        </div>
                        <label class="control-label col-sm-2" for="ultimoNome">Ultimo nome:</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="ultimoNome" placeholder="Preencha campo Ultimo nome" name="ultimoNome">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" for="email">E-mail:</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="email" placeholder="Preencha campo e-mail" name="email">
                        </div>
                        <label class="control-label col-sm-2" for="telefone">Telefone:</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="telefone" placeholder="Preencha campo telefone" name="telefone">
                        </div>
                    </div>

                </div>
            </div>


            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Endere�o</h3>
                </div>
                <div class="panel-body">

                    <div class="form-group">
                        <label class="control-label col-sm-2" for="email">CEP:</label>
                        <div class="col-sm-1">
                            <input type="text" class="form-control" id="email"  name="email">
                        </div>
                        <label class="control-label col-sm-1" for="endereco">Endere�o:</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="endereco" placeholder="Preencha campo endere�o" name="endereco">
                        </div>
                        <label class="control-label col-sm-1" for="distrito">Distrito:</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="distrito" placeholder="Preencha campo distrito" name="distrito">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" for="complemento">Complemento:</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="complemento" placeholder="Preencha campo complemento" name="complemento">
                        </div>
                        <label class="control-label col-sm-1" for="cidade">Cidade:</label>
                        <div class="col-sm-2">
                            <select class="form-control " id="cidade" name="cidade">
                                <option value="0">Selecione...</option>

                               



                            </select>
                        </div>
                        <label class="control-label col-sm-1" for="pais">Pa�s:</label>
                        <div class="col-sm-1">
                            <input type="text" class="form-control" id="pais" name="pais">
                        </div>
                    </div>


                </div>

            </div>





            <div class="form-group">
                <div class="col-sm-offset-10 col-sm-12">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12"> 
                            <input type="submit"   class="btn btn-success col-" value="Salvar" />
                            <span style="margin-left: 12px" />
                            <input type="reset"    class="btn btn-danger" value="Cancelar" />
                        </div>
                    </div>



                </div>
            </div>

        </form> 


    </fieldset>   









    <jsp:include page="../footer.jsp" />